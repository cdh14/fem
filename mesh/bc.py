#!/bin/env python
"""
bc.py

Apply boundary conditions to rectangular solid meshes (the majority of the FE
sims); can handle quarter-, half-, and no symmetry models.

There are 6 faces in these models (c & s meshes can have special cases); we need to:
 1. Find all of them, and
 2. Apply the appropriate BCs

We'll loop through all of the nodes, see if they are on a face or edge, and
then apply the appropriate BC.

Carl's Implementations:
1) - Maintain backwards-compatibility with Mark's original conventions, 
        add handling of Field II convention & allow versatility in 
        rectilinear mesh BC definition
2) - Enable versatile handling of cylindrical & spherical mesh geometries
3) - Accommodate perfect-matching-layer (PML) boundary conditions

EXAMPLE:

=======
Copyright 2015 Mark L. Palmeri (mlp6@duke.edu) and Carl Herickhoff (cdh14@duke.edu)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""


def main():
    import fem_mesh
    import sys

    fem_mesh.check_version()

    opts = read_cli()

    # (Carl's modification: function checks command-line options for validity of geometry & BC's)
    optchk(opts)

    if opts.pml:
        pmlfile = create_pml_elems_file(opts.elefile)

    # open the boundary condition file to write, write header with entered cmd-line args
    BCFILE = open_bcfile(opts, sys.argv[0])
    # load in all of the node data, excluding '*' lines
    nodeIDcoords = fem_mesh.load_nodeIDs_coords(opts.nodefile)

    # there are normally 6 surfaces in these models (c & s meshes can have special cases); we need to:
    # (1) find all of them (by sorting nodes by coordinate dimensions--find min & max), and
    # (2) apply the appropriate BCs 
    # we'll extract the nodes on surfaces that are the min. or max. of each dimension,
    # and then apply the appropriate BC

    # sort nodes according to mesh geometry:
    # (Carl's modification)
    if (opts.crdsys == 'mlp') or (opts.crdsys == 'fld') or (opts.crdsys == 'r'):
        [snic, axes] = fem_mesh.SortNodeIDs(nodeIDcoords) #,opts.xdf) #Original fcn--modified for 'fld' & 'r', diff. xdcr-faces
    elif opts.crdsys == 'c':
        [snic, axes] = fem_mesh.SortCnodeIDs(nodeIDcoords,opts.axrv,opts.xdf) #Carl-modified function
    elif opts.crdsys == 's':
        [snic, axes] = fem_mesh.SortSnodeIDs(nodeIDcoords,opts.axrv,opts.xdf) #Carl-modified function
    else:
        sys.exit("Error: invalid coordinate system specified.")
    #[snic, axes] = fem_mesh.SortNodeIDs(nodeIDcoords)
    axdiff = [axes[0][1]-axes[0][0],
              axes[1][1]-axes[1][0],
              axes[2][1]-axes[2][0]]

    segID = 1

    # BACK
    axis = 0
    axis_limit = axes[0].min()
    planeNodeIDs = fem_mesh.extractPlane(snic, axes, (axis, axis_limit))
    if opts.nonreflect:
        segID = writeSeg(BCFILE, 'BACK', segID, planeNodeIDs)
    elif opts.pml:
        apply_pml(opts.nodefile, pmlfile, BCFILE, planeNodeIDs, axis,
                  axis_limit, axis_limit+opts.num_pml_elems*axdiff[axis],
                  opts.pml_partID)

    # FRONT
    axis = 0
    axis_limit = axes[0].max()
    planeNodeIDs = fem_mesh.extractPlane(snic, axes, (axis, axis_limit))
    if (opts.sym == 'q') or (opts.sym == 'h'):
        # no top / bottom rows (those will be defined in the
        # top/bottom defs)
        writeNodeBC(BCFILE, planeNodeIDs[1:-1], '1,0,0,0,1,1')
    else:
        if opts.nonreflect:
            segID = writeSeg(BCFILE, 'FRONT', segID, planeNodeIDs)
        elif opts.pml:
            apply_pml(opts.nodefile, pmlfile, BCFILE, planeNodeIDs, axis,
                      axis_limit-opts.num_pml_elems*axdiff[axis],
                      axis_limit, opts.pml_partID)

    # LEFT (push side; non-reflecting or symmetry)
    axis = 1
    axis_limit = axes[1].min()
    planeNodeIDs = fem_mesh.extractPlane(snic, axes, (axis, axis_limit))
    # if quarter-symmetry, then apply BCs, in addition to a
    # modified edge; and don't deal w/ top/bottom
    if opts.sym == 'q':
        writeNodeBC(BCFILE, planeNodeIDs[1:-1], '0,1,0,1,0,1')
    # else make it a non-reflecting boundary
    else:
        if opts.nonreflect:
            segID = writeSeg(BCFILE, 'LEFT', segID, planeNodeIDs)
        elif opts.pml:
            apply_pml(opts.nodefile, pmlfile, BCFILE, planeNodeIDs, axis,
                      axis_limit, axis_limit+opts.num_pml_elems*axdiff[axis],
                      opts.pml_partID)

    # RIGHT (non-reflecting)
    axis = 1
    axis_limit = axes[1].max()
    planeNodeIDs = fem_mesh.extractPlane(snic, axes, (axis, axis_limit))
    if opts.nonreflect:
        segID = writeSeg(BCFILE, 'RIGHT', segID, planeNodeIDs)
    elif opts.pml:
        apply_pml(opts.nodefile, pmlfile, BCFILE, planeNodeIDs, axis,
                  axis_limit-opts.num_pml_elems*axdiff[axis],
                  axis_limit, opts.pml_partID)

    # BOTTOM
    axis = 2
    axis_limit = axes[2].min()
    planeNodeIDs = fem_mesh.extractPlane(snic, axes, (axis, axis_limit))
    if opts.nonreflect:
        segID = writeSeg(BCFILE, 'BOTTOM', segID, planeNodeIDs)
        if opts.bottom == 'full':
            writeNodeBC(BCFILE, planeNodeIDs, '1,1,1,1,1,1')
        elif opts.bottom == 'inplane':
            writeNodeBC(BCFILE, planeNodeIDs, '0,0,1,1,1,0')
    elif opts.pml:
        apply_pml(opts.nodefile, pmlfile, BCFILE, planeNodeIDs, axis,
                  axis_limit, axis_limit+opts.num_pml_elems*axdiff[axis],
                  opts.pml_partID)

    # TOP (transducer face)
    axis = 2
    axis_limit = axes[2].max()
    planeNodeIDs = fem_mesh.extractPlane(snic, axes, (axis, axis_limit))
    if opts.nonreflect:
        segID = writeSeg(BCFILE, 'TOP', segID, planeNodeIDs)
        if opts.top:
            writeNodeBC(BCFILE, planeNodeIDs, '1,1,1,1,1,1')
    elif opts.pml:
        apply_pml(opts.nodefile, pmlfile, BCFILE, planeNodeIDs, axis,
                  axis_limit-opts.num_pml_elems*axdiff[axis],
                  axis_limit, opts.pml_partID)

    if opts.nonreflect:
        write_nonreflecting(BCFILE, segID)

    BCFILE.write('*END\n')
    BCFILE.close()



def read_cli():
    """
    read command line arguments
    """
    import argparse as ap

    p = ap.ArgumentParser(description="Generate boundary condition data as "
                          "specified on the command line.",
                          formatter_class=ap.ArgumentDefaultsHelpFormatter)

    p.add_argument("--bcfile",
                   help="boundary condition output file",
                   default="bc.dyn")
    p.add_argument("--nodefile",
                   help="node defintion input file",
                   default="nodes.dyn")
    p.add_argument("--elefile",
                   help="elem defintion input file",
                   default="elems.dyn")
    p.add_argument("--sym",
                   help="mlp symmetry: quarter (q), half (h) symmetry or none (none)",
                   default="q")

    p.add_argument("--pml_partID",
                   help="mlp: part ID to assign to PML",
                   default=2)
    p.add_argument("--num_pml_elems",
                   help="mlp: number of elements in PML (5-10)",
                   default=5)

    p.add_argument("--top",
                   help="mlp: fully constrain top (xdcr surface)",
                   dest='top',
                   action='store_true')
    p.add_argument("--notop",
                   help="mlp: top (xdcr surface) unconstrained",
                   dest='top',
                   action='store_false')
    p.set_defaults(top=True)

    p.add_argument("--bottom",
                   help="mlp: full / inplane constraint of bottom boundary "
                   "(opposite transducer surface) [full, inplane]",
                   default="full")

    s = p.add_mutually_exclusive_group(required=True)
    s.add_argument("--nonreflect",
                   help="mlp: apply non-reflection boundaries",
                   dest='nonreflect',
                   action='store_true')
    s.add_argument("--pml",
                   help="mlp: apply perfect matching layers",
                   dest='pml',
                   action='store_true')
    s.set_defaults(nonreflect=False, pml=False)


    # Carl's added options (backward-compatible with Mark's("mlp") convention)
    p.add_argument("--crdsys",
                   help="intended coord. system (Mark Palmeri's; Field II's; rectilinear; cylindrical; spherical) "
                   "for input mesh [mlp, fld, r, c, s]",
                   default="mlp", type=str)
    p.add_argument("--axrv",
                   help="'axis of revolution' for c or s mesh (theta spins about x-axis, y-axis, or z-axis)"
                   "[x, y, z]", type=str)
    p.add_argument("--xdf",
                   help="transducer-facing face of input mesh: for mlp [PZ], for fld [NZ], "
                   "for r mesh [nx,px,ny,py,NZ,pz], for c mesh [NR,pr,na,pa], for s mesh [nr,PR]", type=str) # , default="pz")

    p.add_argument("--nx", help="BC for min. x-coord. face (symmetry, non-reflecting, fully- or in-plane-constrained--or a combo w/non-reflecting, or perfect-matching-layer) "
                        "for mlp, fld, or r meshes [sym, nr, fc, nrfc, nrip, pml]", default="nr", type=str)
    p.add_argument("--px", help="BC for max. x-coord. face [sym, nr, fc, nrfc, nrip, pml]", default="nr", type=str)
    p.add_argument("--ny", help="BC for min. y-coord. face [sym, nr, fc, nrfc, nrip, pml]", default="nr", type=str)
    p.add_argument("--py", help="BC for max. y-coord. face [sym, nr, fc, nrfc, nrip, pml]", default="nr", type=str)
    p.add_argument("--nz", help="BC for min. z-coord. face [sym, nr, fc, nrfc, nrip, pml]", default="nr", type=str)
    p.add_argument("--pz", help="BC for max. z-coord. face [sym, nr, fc, nrfc, nrip, pml]", default="nr", type=str)

    p.add_argument("--nr", help="BC for min. r-coord. face "
                        "for c or s meshes [nr, fc, nrfc, nrip, pml]", default="nrfc", type=str)
    p.add_argument("--pr", help="BC for max. r-coord. face "
                        "for c or s meshes [nr, fc, nrfc, nrip, pml]", default="nrfc", type=str)

    p.add_argument("--nt", help="BC for min. theta-coord. face "
                        "for c or s meshes [sym, nr, fc, nrfc, nrip, pml]", default="nr", type=str)
    p.add_argument("--pt", help="BC for max. theta-coord. face "
                        "for c or s meshes [sym, nr, fc, nrfc, nrip, pml]", default="nr", type=str)

    p.add_argument("--na", help="BC for min. axial-coord. face "
                        "for c mesh [sym, nr, fc, nrfc, nrip, pml]", default="nr", type=str)
    p.add_argument("--pa", help="BC for max. axial-coord. face "
                        "for c mesh [sym, nr, fc, nrfc, nrip, pml]", default="nr", type=str)

    p.add_argument("--pp", help="BC for max. phi-coord. (90-deg) face "
                        "for s mesh [nr, fc, nrfc, nrip, pml]", default="nr", type=str)

    opts = p.parse_args()

    return opts


def optchk(o):
    # (Carl's addition) This function checks the validity of the options entered on the command line.
    import sys
    import numpy as n
    
    if 'mlp' in o.crdsys: # note: -crdsys = mlp is default
        print("Palmeri convention (force -xdf = pz): only -sym, -top, & -bottom options can be used.")
        o.xdf = 'pz'
        o.oppxdf = 'nz'
        
        o.nx = 'nr'
        o.py = 'nr'

        if 'q' in o.sym: # quarter-symmetry about x=0 & y=0 planes (default)
            print("Quarter symmetry; adjoining edge nodes (x=y=0, minus xdf & opp-xdf ones) only allowed to translate along that line.")
            o.px = 'sym'
            o.ny = 'sym'
        elif 'h' in o.sym: # half-symmetry about x=0 plane only
            print("Half symmetry; no translation or rotation out of x=0 plane.")
            o.px = 'sym'
            o.ny = 'nr'
        elif 'none' in o.sym: # no symmetry boundaries
            print("No symmetry; all side boundaries non-reflecting.")
            o.px = 'nr'
            o.ny = 'nr'
        else: # error
            sys.exit("Error: invalid symmetry condition specified.")
            
        if 'True' in o.top: # fully-constrain (non-reflecting) top boundary (default)
            print("Top boundary fully-constrained (& non-reflecting).")
            o.pz = 'nrfc'
        else: # top boundary is unconstrained (but still non-reflecting)
            print("Top boundary unconstrained (& non-reflecting).")
            o.pz = 'nr'
        
        if 'full' in o.bottom: # fully-constrain (non-reflecting) bottom boundary (default)
            print("Bottom boundary is fully-constrained (& non-reflecting).")
            o.nz = 'nrfc'
        elif 'inplane' in o.bottom: # allow only-in-plane motion on bottom boundary (non-reflecting)
            print("Bottom boundary allows only in-plane motion (& is non-reflecting).")
            o.nz = 'nrip'
        else: # error
            sys.exit("Error: invalid bottom boundary specified.")
            

    elif 'fld' in o.crdsys:
        print("Field II convention (force -xdf = nz): only -px, -nx, -py, & -ny BC's can be specified.")
        o.xdf = 'nz'
        o.oppxdf = 'pz'

        #set any undefined BC's to default (non-reflecting):
        if 'nx' not in o:
            o.nx = 'nr'
        if 'px' not in o:
            o.px = 'nr'
        if 'ny' not in o:
            o.ny = 'nr'
        if 'py' not in o:
            o.py = 'nr'
        if 'nz' not in o:
            o.nz = 'nr'
        if 'pz' not in o:
            o.pz = 'nr'

        if ( ( 'sym' in o.nz ) or ( 'sym' in o.pz ) ):
            sys.exit("Error: xducer-facing face & opposite face cannot be symmetry boundaries!")
        if ( ( 'sym' in o.nx ) and ( 'sym' in o.px ) ):
            sys.exit("Error: opposite (x) faces cannot both be symmetry boundaries!")
        if ( ( 'sym' in o.ny ) and ( 'sym' in o.py ) ):
            sys.exit("Error: opposite (y) faces cannot both be symmetry boundaries!")

        tmpoxdf = o.oppxdf

        if ( ( 'fc' != tmpoxdf ) and ( 'nrfc' != tmpoxdf ) and ( 'nrip' != tmpoxdf ) and ( 'pml' != tmpoxdf ) ):
            sys.exit("Error: opp-xdf face must be fully- or in-plane-constrained, or PML!")


    elif 'r' in o.crdsys:
        print("Rectilinear coordinate system.")

        if 'xdf' not in o: # -xdf not defined!
            print("Warning: -xdf not defined! Setting xdf = nz by default (similar to Field convention), may give error...")
            o.xdf = 'nz'

        #set any undefined BC's to default (non-reflecting):
        if 'nx' not in o:
            o.nx = 'nr'
        if 'px' not in o:
            o.px = 'nr'
        if 'ny' not in o:
            o.ny = 'nr'
        if 'py' not in o:
            o.py = 'nr'
        if 'nz' not in o:
            o.nz = 'nr'
        if 'pz' not in o:
            o.pz = 'nr'

        #set oppxdf:
        if 'nz' in o.xdf:
            o.oppxdf = 'pz'
        elif 'pz' in o.xdf:
            o.oppxdf = 'nz'
        elif 'ny' in o.xdf:
            o.oppxdf = 'py'
        elif 'py' in o.xdf:
            o.oppxdf = 'ny'
        elif 'nx' in o.xdf:
            o.oppxdf = 'px'
        elif 'px' in o.xdf:
            o.oppxdf = 'nx'
        else: # error
            sys.exit("Error: invalid xdf specified.")

        tmpxdf = o.xdf
        tmpoxdf = o.oppxdf

        if ( ('sym' is tmpxdf) or ('sym' is tmpoxdf) ):
            sys.exit("Error: xducer-facing face & opposite face cannot be symmetry boundaries!")

        if ( ( ('sym' in o.nx) and ('sym' in o.px) )  or  ( ('sym' in o.ny) and ('sym' in o.py) )  or  ( ('sym' in o.nz) and ('sym' in o.pz) ) ):
            sys.exit("Error: opposing faces cannot both be symmetry boundaries!")

        if ( (('sym' in o.nx)or('sym' in o.px)) and (('sym' in o.ny)or('sym' in o.py)) or
             (('sym' in o.nx)or('sym' in o.px)) and (('sym' in o.nz)or('sym' in o.pz)) or
             (('sym' in o.ny)or('sym' in o.py)) and (('sym' in o.nz)or('sym' in o.pz))):
            print("Quarter symmetry; adjoining edge nodes (minus xdf & opp-xdf ones) only allowed to translate along that line.")
        elif ( ('sym' in o.nx) or ('sym' in o.px) or ('sym' in o.ny) or ('sym' in o.py) or ('sym' in o.nz) or ('sym' in o.pz) ):
            print("Half symmetry; no translation or rotation out of plane.")
        else:
            print("No symmetry; all side boundaries non-reflecting.")

        if ( ( 'fc' != tmpoxdf ) and ( 'nrfc' != tmpoxdf ) and ( 'nrip' != tmpoxdf ) and ( 'pml' != tmpoxdf ) ):
            sys.exit("Error: opp-xdf face must be fully- or in-plane-constrained, or PML!")


    elif 'c' in o.crdsys:
        print("Cylindrical coordinate system (warning: presence of r=0 edge may cause error).")

        if ( ('x' not in o.axrv) and ('y' not in o.axrv) and ('z' not in o.axrv) ):
            sys.exit("Error: a valid axis of revolution must be specified!")

        if 'axrv' not in o: # -xdf not defined!
            print("Warning: -axrv not defined! Setting axrv = y by default, may give error...")
            o.axrv = 'y'

        if 'xdf' not in o: # -xdf not defined!
            print("Warning: -xdf not defined! Setting xdf = nr by default (outward-transmitting cylindrical xducer), may give error...")
            o.xdf = 'nr'

        #set any undefined BC's to default (non-reflecting):
        if 'nr' not in o:
            o.nr = 'nr'
        if 'pr' not in o:
            if 'nr' in o.xdf:
                o.pr = 'nrfc'
            else:
                o.pr = 'nr'
        if 'nt' not in o:
            o.nt = 'nr'
        if 'pt' not in o:
            o.pt = 'nr'
        if 'na' not in o:
            o.na = 'nr'
        if 'pa' not in o:
            o.pa = 'nr'

        # print o.pr   #purely for de-bugging
        # print o.xdf   #purely for de-bugging

        #set oppxdf:
        if 'nr' in o.xdf:
            o.oppxdf = 'pr'
        elif 'pr' in o.xdf:
            o.oppxdf = 'nr'
        elif 'na' in o.xdf:
            o.oppxdf = 'pa'
        elif 'pa' in o.xdf:
            o.oppxdf = 'na'
        else: # error
            sys.exit("Error: invalid xdf specified (may only be nr, pr, na, or pa for 'c' mesh).")

        tmpxdf = getattr(o,o.xdf)   #BC assigned to the xdf **variable, not object! (use == or !=, not is or in)
        tmpoxdf = getattr(o,o.oppxdf)   #BC assigned to the oppxdf **variable, not object! (use == or !=, not is or in)

        # print o.xdf   #purely for de-bugging
        # print o.oppxdf   #purely for de-bugging
        # print tmpxdf   #purely for de-bugging
        # print tmpoxdf   #purely for de-bugging


        if ( ('sym' is tmpxdf) or ('sym' is tmpoxdf) ):
            sys.exit("Error: xducer-facing face & opposite face cannot be symmetry boundaries!")

        if ( ('sym' in o.nr) or ('sym' in o.pr) ):
            sys.exit("Error: min. & max. r faces cannot be symmetry boundaries!")

        if ( ('sym' in o.na) and ('sym' in o.pa) ):
            sys.exit("Error: opposing axial faces cannot both be symmetry boundaries!")

        if ( ('nt' in o.xdf) or ('pt' in o.xdf) ):
            sys.exit("Error: xducer cannot be on constant-theta plane!")

        if ( ('nr' in o.xdf) or ('pr' in o.xdf) ): # cylindrical xdf (can have eighth symmetry)
            #
            if ( (('sym' in o.na)or('sym' in o.pa)) and ('sym' in o.nt) and ('sym' in o.pt) ):
                print("Eighth symmetry; adjoining edge nodes (minus xdf & opp-xdf ones) only allowed to translate along those lines.")
            elif ( (('sym' not in o.na)and('sym' not in o.pa)) and (('sym' in o.nt)and('sym' in o.pt)) ):
                print("Quarter symmetry via 2 constant-theta faces; no translation or rotation out of those planes.")
            elif ( (('sym' in o.na)or('sym' in o.pa)) and (('sym' in o.nt)or('sym' in o.pt)) ):
                print("Quarter symmetry; adjoining edge nodes (minus xdf & opp-xdf ones) only allowed to translate along that line.")
            elif ( (('sym' in o.na)or('sym' in o.pa)) or ('sym' in o.nt) or ('sym' in o.pt) ):
                print("Half symmetry; no translation or rotation out of plane.")
            else:
                print("No symmetry; all side boundaries non-reflecting.")

            if ( ( 'fc' != tmpoxdf ) and ( 'nrfc' != tmpoxdf ) and ( 'pml' != tmpoxdf ) ):
                sys.exit("Error: for cyl. xdf, opp-xdf face must be fully-constrained, or PML!")

        elif ( ('na' in o.xdf) or ('pa' in o.xdf) ): # circular xdf (can't have eighth symmetry)
            #
            if ( ('sym' in o.nt) and ('sym' in o.pt) ):
                print("Quarter symmetry via 2 constant-theta faces; no translation or rotation out of those planes.")
            elif ( ('sym' in o.nt) or ('sym' in o.pt) ):
                print("Half symmetry; no translation or rotation out of plane.")
            else:
                print("No symmetry; all side boundaries non-reflecting.")

            if ( 'nrfc' != tmpoxdf ):
                print tmpoxdf
                print("wtf is that?")
            if ( ( 'fc' != tmpoxdf ) and ( 'nrfc' != tmpoxdf ) and ( 'nrip' != tmpoxdf ) and ( 'pml' != tmpoxdf ) ):
                sys.exit("Error: opp-xdf face must be fully- or in-plane-constrained, or PML!")


    elif 's' in o.crdsys:
        print("Spherical coordinate system (warning: r=0 vertex may cause error).")

        if ( ('x' not in o.axrv) and ('y' not in o.axrv) and ('z' not in o.axrv) ):
            sys.exit("Error: a valid axis of revolution must be specified!")

        if 'xdf' not in o: # -xdf not defined!
            print("Warning: -xdf not defined! Setting xdf = pr by default (inward-transmitting spherical xducer), may give error...")
            o.xdf = 'pr'

        if 'axrv' not in o: # -xdf not defined!
            print("Warning: -axrv not defined! Setting axrv = y by default, may give error...")
            o.axrv = 'y'

        #set any undefined BC's to default (non-reflecting):
        if 'nr' not in o:
            o.nr = 'nr'
        if 'pr' not in o:
            o.pr = 'nr'
        if 'nt' not in o:
            o.nt = 'nr'
        if 'pt' not in o:
            o.pt = 'nr'

        o.np = 'nr'   # dummy variable (phi=0 is an edge, not a face)
        if 'pp' not in o:
            o.pp = 'nr'

        #set oppxdf:
        if 'pr' in o.xdf:
            o.oppxdf = 'nr'
        elif 'nr' in o.xdf:
            o.oppxdf = 'pr'
        else: # error
            sys.exit("Error: invalid xdf specified (may only be nr or pr for 's' mesh).")

        tmpxdf = getattr(o,o.xdf) #o.xdf
        tmpoxdf = getattr(o,o.oppxdf) #o.oppxdf

        if ( ('sym' is tmpxdf) or ('sym' is tmpoxdf) ):
            sys.exit("Error: xducer-facing face & opposite face cannot be symmetry boundaries!")

        if ( ('sym' in o.nr) or ('sym' in o.pr) ):
            sys.exit("Error: min. & max. r faces cannot be symmetry boundaries!")

        if ('sym' in o.pp):
            sys.exit("Error: max. phi (90-deg) face cannot be symmetry boundary!")

        if ( ('nt' in o.xdf) or ('pt' in o.xdf) or ('pp' in o.xdf)):
            sys.exit("Error: xducer cannot be on constant-theta or constant-phi planes!")

        if ( ('sym' in o.nt) and ('sym' in o.pt) ):
            print("Quarter symmetry via 2 constant-theta faces; no translation or rotation out of those planes.")
        elif ( ('sym' in o.nt) or ('sym' in o.pt) ):
            print("Half symmetry; no translation or rotation out of plane.")
        else:
            print("No symmetry; all side boundaries non-reflecting.")

        if ( ( 'fc' != tmpoxdf ) and ( 'nrfc' != tmpoxdf ) and ( 'pml' != tmpoxdf ) ):
            sys.exit("Error: for spherical xdf, opp-xdf face must be fully-constrained, or PML!")


def create_pml_elems_file(elefile):
    """
    create a new output elements file that the PML elements will be defined in
    that has _pml added to the filename.  elefile is assumed to end in '.dyn'

    this could be a homogeneous elems.dyn file, or a struct.dyn file

    EXAMPLE:
        elems.dyn -> elems_pml.dyn
    """
    import shutil

    pmlfile = elefile.replace('.dyn', '_pml.dyn')

    shutil.copy(elefile, pmlfile)

    return pmlfile


def open_bcfile(opts, cmdline):
    """
    open BC file for writing and write header with command line
    """
    BCFILE = open(opts.bcfile, 'w')
    BCFILE.write("$ Generated using %s with the following options:\n" %
                 cmdline)
    BCFILE.write("$ %s\n" % opts)
    return BCFILE


def writeSeg(BCFILE, title, segID, planeNodeIDs):
    BCFILE.write('*SET_SEGMENT_TITLE\n')
    BCFILE.write('%s\n' % title)
    BCFILE.write('%i\n' % segID)
    for i in range(0, (len(planeNodeIDs) - 1)):
        (a, b) = planeNodeIDs.shape
        for j in range(0, (b - 1)):
            BCFILE.write("%i,%i,%i,%i\n" % (planeNodeIDs[i, j][0],
                                            planeNodeIDs[i + 1, j][0],
                                            planeNodeIDs[i + 1, j + 1][0],
                                            planeNodeIDs[i, j + 1][0]))
    segID = segID + 1
    return segID


def writeNodeBC(BCFILE, planeNodeIDs, dofs):
    BCFILE.write('*BOUNDARY_SPC_NODE\n')
    # don't grab the top / bottom rows (those will be defined in the top/bottom
    # defs)
    for i in planeNodeIDs:
        for j in i:
            BCFILE.write("%i,0,%s\n" % (j[0], dofs))


def apply_pml(nodefile, pmlfile, BCFILE, planeNodeIDs, axis, axmin, axmax,
              pml_partID):
    """
    Apply full nodal constraints to the outer face nodes and then create outer
    layers that are assigned to *MAT_PML_ELASTIC.
    """
    import CreateStructure as CS

    writeNodeBC(BCFILE, planeNodeIDs, '1,1,1,1,1,1')

    structNodeIDs = CS.findStructNodeIDs(nodefile,
                                         'layer',
                                         (axis+1, axmin, axmax))

    (elems, structElemIDs) = CS.findStructElemIDs(pmlfile,
                                                  structNodeIDs)

    CS.write_struct_elems(pmlfile, pml_partID, elems, structNodeIDs,
                          structElemIDs)


def write_nonreflecting(BCFILE, segID):
    """
    write non-reflecting boundaries (set segment references)
    """
    BCFILE.write('*BOUNDARY_NON_REFLECTING\n')
    for i in range(1, segID):
        BCFILE.write('%i,0.0,0.0\n' % i)

def CSbcstr(axrv,tmax,case,bc):
    # determine & return proper x/y/z translation, x/y/z rotation constraint string
    # for cylindrical & spherical meshes based on axrv & max. theta coordinate
    # (recall: min. theta forced to = 0)\
    import sys
    import numpy as n
    import fem_mesh

    if (tmax > 180): # max. t is...?
        sys.exit("Error:...haven't figured out how to handle this full 'disk' geometry yet...")

    bcstr=[]  # initialize empty array
    # determine min. & max. theta-plane constraint strings:
    if 'x' is axrv:
        bcstr.append( fem_mesh.constrain('z',bc[0],False) )
        if (90 is tmax): # max. t is y=0 plane
            bcstr.append( fem_mesh.constrain('y',bc[1],False) )
        elif (180 is tmax): # max. t is also z=0 plane => apply (force) same BC's as to min. t
            bcstr.append( fem_mesh.constrain('z',bc[0],False) ) 
    elif 'y' is axrv:
        bcstr.append( fem_mesh.constrain('x',bc[0],False) )
        if (90 is tmax): # max. t is z=0 plane
            bcstr.append( fem_mesh.constrain('z',bc[1],False) )
        elif (180 is tmax): # max. t is also x=0 plane => apply (force) same BC's as to min. t
            bcstr.append( fem_mesh.constrain('x',bc[0],False) ) 
    elif 'z' is axrv:
        bcstr.append( fem_mesh.constrain('y',bc[0],False) )
        if (90 is tmax): # max. t is x=0 plane
            bcstr.append( fem_mesh.constrain('x',bc[1],False) )
        elif (180 is tmax): # max. t is also y=0 plane => apply (force) same BC's as to min. t
            bcstr.append( fem_mesh.constrain('y',bc[0],False) ) 

    if ( ('PA' is case) or ('PP' is case) ): # transducer on an r face
        bcstr.append( fem_mesh.constrain(axrv,bc[2],False) )
        bcstr.append( fem_mesh.constrain(axrv,bc[3],False) )
        bcstr.append( fem_mesh.constrain(axrv,bc[4],True) )
        bcstr.append( fem_mesh.constrain(axrv,bc[5],True) )
    
    elif 'PR' is case: # transducer on an axial face
        bcstr.append( fem_mesh.constrain(axrv,bc[2],True) )
        bcstr.append( fem_mesh.constrain(axrv,bc[3],True) )
        bcstr.append( fem_mesh.constrain(axrv,bc[4],False) )
        bcstr.append( fem_mesh.constrain(axrv,bc[5],False) )

    return bcstr



if __name__ == "__main__":
    main()
