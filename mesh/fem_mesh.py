"""
fem_mesh.py

Generic functions for all meshing functions

Copyright 2015 Mark L. Palmeri (mlp6@duke.edu)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
from __future__ import absolute_import
from __future__ import print_function


def check_version():
    """
    Check that at least python2.7 is being used for argparse compatibility
    """
    import sys

    if sys.version_info[:2] < (2, 7):
        sys.exit("ERROR: Requires Python >= 2.7")


def strip_comments(nodefile):
    import os
    nodefile_nocmt = '%s.tmp' % nodefile
    os.system("egrep -v '(^\*|^\$)' %s > %s" % (nodefile, nodefile_nocmt))
    return nodefile_nocmt


def count_header_comment_skips(nodefile):
    """
    count the number of file head comments lines to skip before the first
    keyword (line starting with *)
    """
    import re
    node = re.compile('\*')
    count = 1
    with open(nodefile) as f:
        for line in f:
            if node.match(line):
                return count
            else:
                count = count + 1


def load_nodeIDs_coords(nodefile):
    """
    load in node IDs and coordinates, excluding '*' keyword lines
    """
    import fem_mesh
    import numpy as n
    header_comment_skips = fem_mesh.count_header_comment_skips(nodefile)
    nodeIDcoords = n.loadtxt(nodefile,
                             delimiter=',',
                             comments='*',
                             skiprows=header_comment_skips,
                             dtype=[('id', 'i4'), ('x', 'f4'), ('y', 'f4'),
                                    ('z', 'f4')])
    return nodeIDcoords


def rm_tmp_file(nodefile_nocmt):
    import os
    try:
        os.remove(nodefile_nocmt)
    except OSError as e:
        print(('ERROR: %s - %s.' % (e.argsfilename, e.argsstrerror)))


def extractPlane(snic, axes, plane):
    '''
    Extract the node IDs on a specified plane from a sorted node ID &
    coordinate 3D array.

    INPUTS:
        snic - sorted node IDs & coordinates array
        axes - list of unique coordinates in the x, y, and z dimensions
        plane - list:
            index - index of the plane to extract (x=0, y=1, z=2)
            coord - coordinate of the plane to extract (must exist in axes
                    list)

    OUPUTS:
        planeNodeIDs - spatially-sorted 2D node IDs on the specified plane

    EXAMPLE: planeNodeIDs = extractPlane(snic,axes,(0,-0.1))
    '''
    import sys

    if plane[0] == 0:
        planeNodeIDs = snic[axes[plane[0]] == plane[1], :, :]
    elif plane[0] == 1:
        planeNodeIDs = snic[:, axes[plane[0]] == plane[1], :]
    elif plane[0] == 2:
        planeNodeIDs = snic[:, :, axes[plane[0]] == plane[1]]
    else:
        sys.exit("ERROR: Specified plane index to extract does not exist")

    planeNodeIDs = planeNodeIDs.squeeze()
    return planeNodeIDs


def SortNodeIDs(nic):
    '''
    Sort the node IDs by spatial coordinates into a 3D matrix and return the
    corresponding axes

    INPUTS:
        nic - nodeIDcoords (n matrix [# nodes x 4, dtype = i4,f4,f4,f4])

    OUTPUTS:
        SortedNodeIDs - n matrix (x,y,z)
        x - array
        y - array
        z - array
    '''

    import sys
    import numpy as n

    axes = [n.unique(nic['x']), n.unique(nic['y']), n.unique(nic['z'])]

    # test to make sure that we have the right dimension (and that precision
    # issues aren't adding extra unique values)
    if len(nic) != (axes[0].size * axes[1].size * axes[2].size):
        sys.exit('ERROR: Dimension mismatch - possible precision error '
                 'when sorting nodes (?)')

    # sort the nodes by x, y, then z columns
    I = nic.argsort(order=('x', 'y', 'z'))
    snic = nic[I]
    snic = snic.reshape((axes[0].size, axes[1].size, axes[2].size))

    return [snic, axes]

'''
def SortNodeIDs(nic,xdf):
    
    Sort the node IDs by spatial coordinates into a 3D matrix and return the
    corresponding axes

    INPUTS:
        nic - nodeIDcoords (n matrix [# nodes x 4, dtype = i4,f4,f4,f4])

    OUTPUTS*:
        SortedNodeIDs - n matrix (x,y,z)
        x - array
        y - array
        z - array
        (* = order may differ for 'r' mesh & xdf = nx/px/ny/py)
    

    import sys
    import numpy as n

    if ( ('pz' is xdf) or ('nz' is xdf) ): # mlp, fld, or [r mesh w/xdf = nz or pz]
        axes = [n.unique(nic['x']), n.unique(nic['y']), n.unique(nic['z'])]
        I = nic.argsort(order=('x', 'y', 'z'))

    elif ( ('px' is xdf) or ('nx' is xdf) ):
        axes = [n.unique(nic['y']), n.unique(nic['z']), n.unique(nic['x'])]
        I = nic.argsort(order=('y', 'z', 'x'))
    elif ( ('py' is xdf) or ('ny' is xdf) ):
        axes = [n.unique(nic['x']), n.unique(nic['z']), n.unique(nic['y'])]
        I = nic.argsort(order=('x', 'z', 'y'))

    else: # error
        sys.exit("Error: invalid xdf specified.")

    
    # test to make sure that we have the right dimension (and that precision
    # issues aren't adding extra unique values)
    if len(nic) != (axes[0].size * axes[1].size * axes[2].size):
        sys.exit('ERROR: Dimension mismatch - possible precision error '
                 'when sorting nodes (?)')

    # # sort the nodes by x, y, then z columns
    # I = nic.argsort(order=('x', 'y', 'z'))
    snic = nic[I]
    snic = snic.reshape((axes[0].size, axes[1].size, axes[2].size))

    return [snic, axes]
'''
def SortCnodeIDs(nic,axrv,xdf): #Carl's addition for cylindrical mesh
    '''
    Calculate nodal r & theta coordinates, sort the node IDs by spatial 
    coordinates into a 3D matrix, and return the corresponding axes

    INPUTS:
        nic - nodeIDcoords (n matrix [# nodes x 4, dtype = i4,f4,f4,f4])
        axrv - 'axis of revolution' (theta spins about x, y, or z axis)

    OUTPUTS*:
        SortedNodeIDs - n matrix (r,theta,ax)
        r - array
        theta - array
        ax - array
        (* = in order of t/a/r for xdf=nr/pr, or t/r/a for xdf=na/pa)
    '''

    import sys
    import numpy as n
    # print nic.size
    # print nic[0][3]

    snictmp = n.zeros((nic.size), dtype=[('id', 'i4'), ('x', 'f4'), ('y', 'f4'), ('z', 'f4'),
                                                            ('r', 'f4'), ('t', 'f4'), ('a', 'f4')])
    # print snictmp[0]
    # print snictmp[nic.size]

    # (round r and a to 3-decimal-place precision, and t to 1-decimal-place precision)
    if 'x' is axrv:  # y-axis = theta zero
        for i in range(0, nic.size):
            r = n.sqrt( nic[i][2]**2 + nic[i][3]**2 )
            t = n.degrees( n.arctan2( nic[i][3], nic[i][2] ) )
            if (t<0):
                t = t+360
            a = nic[i][1]
            snictmp[i] = ( nic[i][0], nic[i][1], nic[i][2], nic[i][3], round(r,3), round(t,1), round(a,3) )

    elif 'y' is axrv:  # z-axis = theta zero
        for i in range(0, nic.size):
            r = n.sqrt( nic[i][3]**2 + nic[i][1]**2 )
            t = n.degrees( n.arctan2( nic[i][1], nic[i][3] ) )
            if (t<0):
                t = t+360
            a = nic[i][2]
            snictmp[i] = ( nic[i][0], nic[i][1], nic[i][2], nic[i][3], round(r,3), round(t,1), round(a,3) )

    elif 'z' is axrv:  # x-axis = theta zero
        for i in range(0, nic.size):
            r = n.sqrt( nic[i][1]**2 + nic[i][2]**2 )
            t = n.degrees( n.arctan2( nic[i][2], nic[i][1] ) )
            if (t<0):
                t = t+360
            a = nic[i][3]
            snictmp[i] = ( nic[i][0], nic[i][1], nic[i][2], nic[i][3], round(r,3), round(t,1), round(a,3) )

    else: # error
        sys.exit("Error: invalid axis of revolution specified.")

    if ( 0 is min(n.unique(snictmp['r'])) ):
        print("Warning: minimum r = 0 may result in errors!...")

    # print snictmp[0]
    # print n.unique(snictmp['t'])

    mint = min(n.unique(snictmp['t']))
    # print mint
    if ( mint not in [0,90,180,270] ):                                              # ( 0 is not min(n.unique(snictmp['t'])) ):
        sys.exit("Error: invalid min. theta value (must = 0, 90, 180, or 270).")    # ("Error: minimum theta value must be = 0.")

    maxt = max(n.unique(snictmp['t']))
    # print maxt
    if ( maxt not in [90,180,270,360] ): #purely for de-bugging qtr pipe in crazy mlp coord-sys # (90 is not maxt) or (180 is not maxt) or (270 is not maxt) or (360 is not maxt) ):
        sys.exit("Error: invalid max. theta value (must = 90, 180, or ~360).")
    # elif (180 is maxt):
    #     print("Warning: for half-cylinder, min. & max. theta faces are the same plane => same BC's will be applied.")

    trange = ( maxt - mint ) #purely for de-bugging qtr pipe in crazy mlp coord-sys
    # print trange #purely for de-bugging qtr pipe in crazy mlp coord-sys
    if (trange not in [90,180,360]):
        sys.exit("Error: invalid theta range (must be 90, 180, or ~360).")
    if (180 is trange):
        print("Warning: for half-cylinder, min. & max. theta faces are the same plane => same BC's will be applied.")

    # print xdf #purely for de-bugging qtr pipe in crazy mlp coord-sys

    # axes = [n.unique(nic['x']), n.unique(nic['y']), n.unique(nic['z'])]
    # # sort the nodes by x, y, then z columns
    # I = nic.argsort(order=('x', 'y', 'z'))
    # sort the nodes by r/t/a columns depending on xdf
    if ( ('pr' in xdf) or ('nr' in xdf) ):
        axes = [n.unique(snictmp['t']), n.unique(snictmp['a']), n.unique(snictmp['r'])]
        I = snictmp.argsort(order=('t', 'a', 'r'))
    elif ( ('pa' in xdf) or ('na' in xdf) ):
        axes = [n.unique(snictmp['t']), n.unique(snictmp['r']), n.unique(snictmp['a'])]
        I = snictmp.argsort(order=('t', 'r', 'a'))
    else: # error
        sys.exit("Error: invalid xdf specified.")

    # test to make sure that we have the right dimension (and that precision
    # issues aren't adding extra unique values)
    if len(nic) != (axes[0].size * axes[1].size * axes[2].size):
        sys.exit('ERROR: Dimension mismatch - possible precision error '
                 'when sorting nodes (?)')

    # snic = nic[I]
    snic = snictmp[I]

    snic = snic.reshape((axes[0].size, axes[1].size, axes[2].size))

    return [snic, axes]


def SortSnodeIDs(nic,axrv,xdf): #Carl's addition for spherical mesh
    '''
    Calculate nodal r, theta, & phi coordinates, sort the node IDs by spatial 
    coordinates into a 3D matrix, and return the corresponding axes

    INPUTS:
        nic - nodeIDcoords (n matrix [# nodes x 4, dtype = i4,f4,f4,f4])
        axrv - 'axis of revolution' (theta spins about x, y, or z axis)

    OUTPUTS*:
        SortedNodeIDs - n matrix (r,theta,phi)
        r - array
        theta - array
        phi - array
        (* = in order of t/p/r )
    '''

    import sys
    import numpy as n

    snictmp = n.zeros((nic.size), dtype=[('id', 'i4'), ('x', 'f4'), ('y', 'f4'), ('z', 'f4'),
                                                            ('r', 'f4'), ('t', 'f4'), ('p', 'f4')])

    # (round r to 3-decimal-place precision, and t and p to 1-decimal-place precision)
    if 'x' in axrv:  # y-axis = theta zero
        for i in range(0, nic.size):
            r = n.sqrt( nic[i][1]**2 + nic[i][2]**2 + nic[i][3]**2 )
            t = n.degrees( n.arctan2( nic[i][3], nic[i][2] ) )
            if (t<0):
                t = t+360
            p = n.acos( nic[i][1] / r )
            snictmp[i] = ( nic[i][0], nic[i][1], nic[i][2], nic[i][3], round(r,3), round(t,1), round(p,1) )

    elif 'y' in axrv:  # z-axis = theta zero
        for i in range(0, nic.size):
            r = n.sqrt( nic[i][1]**2 + nic[i][2]**2 + nic[i][3]**2 )
            t = n.degrees( n.arctan2( nic[i][1], nic[i][3] ) )
            if (t<0):
                t = t+360
            p = n.acos( nic[i][2] / r )
            snictmp[i] = ( nic[i][0], nic[i][1], nic[i][2], nic[i][3], round(r,3), round(t,1), round(p,1) )

    elif 'z' in axrv:  # x-axis = theta zero
        for i in range(0, nic.size):
            r = n.sqrt( nic[i][1]**2 + nic[i][2]**2 + nic[i][3]**2 )
            t = n.degrees( n.arctan2( nic[i][2], nic[i][1] ) )
            if (t<0):
                t = t+360
            p = n.acos( nic[i][3] / r )
            snictmp[i] = ( nic[i][0], nic[i][1], nic[i][2], nic[i][3], round(r,3), round(t,1), round(p,1) )

    else: # error
        sys.exit("Error: invalid axis of revolution specified.")

    if ( 0 is min(n.unique(snictmp['r'])) ):
        print("Warning: minimum r = 0 may result in errors!...")

    if ( 0 is not min(n.unique(snictmp['t'])) ):
        sys.exit("Error: minimum theta value must be = 0.")

    maxt = max(n.unique(snictmp['t']))
    if ( (maxt < 315) and ((90 is not maxt) or (180 is not maxt)) ):
        sys.exit("Error: invalid max. theta value (must = 90, 180, or ~360).")

    if ( (0 is not min(n.unique(snictmp['p']))) or (90 is not max(n.unique(snictmp['p']))) ):
        sys.exit("Error: invalid range of phi values ([min,max] must = [0,90]).")

    # axes = [n.unique(nic['x']), n.unique(nic['y']), n.unique(nic['z'])]
    axes = [n.unique(snictmp['t']), n.unique(snictmp['p']), n.unique(snictmp['r'])]

    # test to make sure that we have the right dimension (and that precision
    # issues aren't adding extra unique values)
    if len(nic) != (axes[0].size * axes[1].size * axes[2].size):
        sys.exit('ERROR: Dimension mismatch - possible precision error '
                 'when sorting nodes (?)')

    # # sort the nodes by x, y, then z columns
    # I = nic.argsort(order=('x', 'y', 'z'))
    # sort the nodes by t, p, then r columns
    I = snictmp.argsort(order=('t', 'p', 'r'))

    # snic = nic[I]
    snic = snictmp[I]

    snic = snic.reshape((axes[0].size, axes[1].size, axes[2].size))

    return [snic, axes]


def constrain(face,BC,r):
    '''
    Determine & return proper x/y/z translation, x/y/z rotation constraint string

    INPUTS:
        face - [x, y, z] as face to which BCs will be applied (or as axis of revolution in case of cyl/sph mesh)
        BC - [sym, nr, fc, nrfc, nrip] as type of BC to be applied to particular face
        r - [True, False] flag for constant-r faces, to be treated differently (nodes on face won't have constant x, y, or z coord)
    OUTPUTS:
        bcstr - string of 1/0's (constrained/unconstrained) for [x-trans,y-trans,z-trans,x-rot,y-rot,z-rot]
    '''
    import sys
    import numpy as n

    # print BC #purely for de-bugging 

    if (r is True): # NOTE:**'face' = axrv** for constant-r face of cylindrical mesh!
        if ('nrip' in BC): # recall: r-faces can't be symmetry boundaries
            if ('x' is face):
                bcstr = '0,1,1,0,1,1'
            elif ('y' is face):
                bcstr = '1,0,1,1,0,1'
            elif ('z' is face):
                bcstr = '1,1,0,1,1,0'
        elif ( ('nrfc' in BC) or ('fc' in BC) or ('pml' in BC) ):
            bcstr = '1,1,1,1,1,1'
        elif ('nr' in BC):
            bcstr = '0,0,0,0,0,0' # note: placeholder; doesn't actually get written to BCFILE
        else:
            # print 'r is true' #purely for de-bugging
            sys.exit("Error: invalid boundary condition specified.")

    else: # (all other types of faces)
        if ( ('sym' in BC) or ('nrip' in BC) ):
            # print 'in sym/nrip loop...'  #purely for de-bugging
            if ('x' is face):
                bcstr = '1,0,0,0,1,1'
            elif ('y' is face):
                bcstr = '0,1,0,1,0,1'
            elif ('z' is face):
                bcstr = '0,0,1,1,1,0'
        elif ( ('nrfc' in BC) or ('fc' in BC) or ('pml' in BC) ):
            bcstr = '1,1,1,1,1,1'
        elif ('nr' in BC):
            bcstr = '0,0,0,0,0,0' # note: placeholder; doesn't actually get written to BCFILE
        else:
            # print 'r is False' #purely for de-bugging
            sys.exit("Error: invalid boundary condition specified.")

    return bcstr
